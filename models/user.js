var mongoose = require('mongoose');

// user schema
let userSchema = mongoose.Schema({
    display_name : {
        type: String,
        required: false
    },
    phone_no: {
        type: String,
        required: true
    },
    ext: {
        type: String,
        required: true
    },
    display_picture : {
        type: String,
        required: false
    },
    verified : {
        type: Boolean,
        required: false
    },
    otp : {
        type: Number,
        required: false
    },
    otp_expiry : {
        type: Date,
        required: false
    },
    created_at : {
        type: Date,
    },
    updated_at : {
        type: Date,
        default: Date.now
    },
    last_active : {
        type: Date,
        default: Date.now
    }
});

// define model and export
let User = module.exports = mongoose.model('User',userSchema);