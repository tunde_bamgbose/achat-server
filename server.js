// load config file
const config = require('./app/config');

// create variable to holde express
const express = require('express');
const app = express();
const port = config.app.port;

// use body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// bring in randomstring
var randomstring = require('randomstring');

// bring in models
const User = require('./models/user');

// connect to monogodb
const mongoose = require('mongoose');
mongoose.connect(`mongodb://${config.app.db.username}:${config.app.db.password}@ds233806.mlab.com:33806/achat`,{useNewUrlParser: true})

// let db = mongoose.connection;
// db.once('open',()=>{
//     console.log("Database connected");

//     // create server and assign port if db is connected
//     app.listen(port, () => {
//         console.log(`Server started on port ${port}`);
//     });
// });

// db.on('error',(err)=>{
//     console.log("Database connection error: ",err);
// })

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});

// define routes here
app.get('/', (req, res) => {
    res.send("Hello World");
});


/**
 * @api {post} /register Register or Login to Account
 * @apiName Register
 * @apiGroup Account
 */
app.post('/register', async (req, res) => {
    var user = new User(req.body);

    // create expiry time for otp
    var today = new Date();
    today.setMinutes(today.getMinutes() + config.app.otp_expiry);

    try{
        // check if user already exist
        let oldUser = await User.findOne({ "phone_no": user.phone_no, "ext": user.ext});
        if(oldUser){
            console.log('old user');
            // reset otp and otp_expiry for user
            oldUser.otp = randomstring.generate({
                length: 5,
                charset: 'numeric'
            });

            oldUser.otp_expiry = today;
            oldUser.updated_at = Date.now();
  
            let me = await oldUser.save();
            let success = {
                "status": true,
                "message": "Please enter code sent to "+user.ext+''+user.phone_no,
                "data": {
                    "phone_no": user.phone_no,
                    "ext": user.ext
                }
            };

            res.send(success);  
        }
        else{
            // add date joined
            user.created_at = Date.now();

            // add otp and otp expiry time
            user.otp = randomstring.generate({
                length: 5,
                charset: 'numeric'
            });

            user.otp_expiry = today;

            // create new user and throw an exception if error exists
            let me = await user.save();
            let success = {
                "status": true,
                "message": "Please enter code sent to "+user.ext+''+user.phone_no,
                "data": {
                    "phone_no": user.phone_no,
                    "ext": user.ext
                }
            };

            res.send(success);  
        }
    }
    catch(err){
        err.status = false,
        res.status(400).send(err);
    }
    
});

/**
 * @api {get} /users List All User Account
 * @apiName List Accounts
 * @apiGroup Account
 */
app.get('/users',async (req,res)=>{
    try{
        let users = await User.find();
        let result = {
            total : users.length,
            data: users
        }
        res.send(result);
    }
    catch(err){
        res.status(500).send(err);
    }
});

/**
 * @api {delete} /users/:id Delete User Account
 * @apiName Delete Account
 * @apiGroup Account
 */
app.delete('/users/:id?', async (req, res) => {
    if(req.params.id)
        await User.deleteOne({ "_id": req.params.id});
    else
        await User.deleteMany();
    res.send({ "status": true, "message": "Request Completed"});
});

/**
 * @api {put} /users/:id Update User Account
 * @apiName Update Account
 * @apiGroup Account
 */
app.put('/users/:id', async (req, res) => {
    try{
        let user = {};
        if(req.body.display_name)
            user.display_name = req.body.display_name;
        if(req.body.display_picture)
            user.display_picture = req.body.display_picture;
        let me = await User.updateOne({"_id": req.params.id},user);
        let success = {
            status: true,
            message: "Account updated!"
        };
        res.send(success);
    }
    catch(err){
        err.status = false;
        res.status(500).send(err);
    }
});

/**
 * @api {put} /otp Generate OTP for User Account
 * @apiName Generate OTP
 * @apiGroup Account
 */
app.put('/otp', async (req, res) => {
    try{
        let user = await User.findOne({ "phone_no": req.body.phone_no, "ext": req.body.ext});
        if(user){
            // reset otp and otp_expiry for user
            // reset otp and otp_expiry for user
            let oldUser = {};
            oldUser.otp = randomstring.generate({
                length: 5,
                charset: 'numeric'
            });

            var today =  new Date();
            today.setMinutes(today.getMinutes() + config.app.otp_expiry);
            oldUser.otp_expiry = today;
            oldUser.updated_at = Date.now();

            // create new user and throw an exception if error exists
            let me = await User.updateOne({"_id": user._id},oldUser);
            let success = {
                "status": true,
                "message": "Please enter code sent to "+user.ext+''+user.phone_no,
                "data": {
                    "phone_no": user.phone_no,
                    "ext": user.ext
                }
            };

            res.send(success); 
        }
        else{
            let error = {
                "status": false,
                "message": "Account not found"
            };
            res.status(404).send(error);
        }
    }
    catch(err){
        err.status = false;
        res.status(500).send(err);
    }
});

/**
 * @api {post} /otp Verify OTP for User Account
 * @apiName Verify OTP
 * @apiGroup Account
 */
app.post('/otp', async (req, res) => {
    try{
        let user = await User.findOne({ "phone_no": req.body.phone_no, "ext": req.body.ext, "otp": req.body.otp},"_id otp otp_expiry verified");
        if(user){
            // check if otp has expired
            let now = new Date().getTime();
            let expiry_time = new Date(user.otp_expiry).getTime();

            if(now > expiry_time){
                let error = {
                    "status": false,
                    "message": "Code expired. Please try again"
                };
                res.status(401).send(error);
            }
            else{
                user.verified = true;
                user.otp = null;
                let me = await user.save();
                let success = {
                    "status": true,
                    "message": "Account verified",
                    "data": {
                        "phone_no": req.body.phone_no,
                        "ext": req.body.ext,
                        "id": user._id 
                    }
                };
                res.send(success); 
            }                
        }
        else{
            let error = {
                "status": false,
                "message": "Invalid code entered for "+req.body.ext+''+req.body.phone_no
            };
            res.status(404).send(error);
        }
    }
    catch(err){
        err.status = false;
        res.status(400).send(err);
    }
});

/**
 * @api {post} /search/:id/contacts Find contacts on Platfom
 * @apiName Search Contacts
 * @apiGroup Contacts
 */
app.post('/search/:id/contacts', async (req, res) => {
    // this route searches the platform for contacts already registered
    // users contacts will be sent to this route as an array
    let contacts = req.body.contacts ? req.body.contacts : [];

    // go through each contact and find user on platform
    let list = []; // hold new list

    try{

        // check if user has already been established as a VERIFIED platform user
        // skip process if user is registered

        for(const contact of contacts){
            if(contact["_id"] === undefined){
                // find user with phone number
                let user = await User.findOne({"phone_no": contact.phone_no},"_id created_at last_active display_picture");
                if(user)
                    list.push(user);
                else
                    list.push(contact);
            }
            else{
                // contact is already registered. copy contact into new list
                list.push(contact);
            } 
        }

        // wait until all promises are resolved
        let response = {
            status: true,
            data: list
        }
        res.send(response);
    }
    catch(err){
        err.status = false;
        res.status(400).send(err);
    }
});

